csvFolder = 'populationHistories/csv';
epsFolder = 'populationHistories/eps';
populationHistoriesFiles = dir(fullfile(csvFolder, '*.csv'));

sampling = 5;
timeStep = 0.02;
historySize = 30000;

for populationHistoryFile=populationHistoriesFiles'
    populationHistory = table2array(readtable(fullfile(csvFolder, populationHistoryFile.name)));
    if(size(populationHistory, 1) > historySize)
        populationHistory = populationHistory(1:historySize, :);
    end
    
    x = 0:timeStep:(size(populationHistory, 1) - 1) * timeStep;
%     figure
    plot(x(1:sampling:end), populationHistory(1:sampling:end, 1)', 'g', ...
        x(1:sampling:end), populationHistory(1:sampling:end, 2)', 'y', ...
        x(1:sampling:end), populationHistory(1:sampling:end, 3)', 'r');
    legend('Rośliny', 'Roślinożercy', 'Mięsożercy');
    xlabel('Czas [s]');
    ylabel('Populacja');
%     title(populationHistoryFile.name(1:end-4))

    print(fullfile(epsFolder, populationHistoryFile.name(1:end-4)), '-depsc'); 
end


