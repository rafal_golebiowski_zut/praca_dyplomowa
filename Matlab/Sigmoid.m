x = linspace(-10, 10, 1000);

y = 1 ./ (1 + exp(-x));

plot(x, y);
ylim([-0.02, 1.02]);
print -depsc Sigmoid