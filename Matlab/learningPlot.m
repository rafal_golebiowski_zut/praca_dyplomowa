csvFolder = 'learning/csv';
epsFolder = 'learning/eps';

timeStep = 0.04;
smoothing = 0.8;
alpha = 0.2;
lineWidth = 0.5;

baseTrainingNames = {'run_Engineering_existentialReward_3_0_CarnivoreBrain-tag-Environment_Episode Length.csv', ...
    'run_Engineering_existentialReward_3_0_HerbivoreBrain-tag-Environment_Episode Length.csv'};
baseHerbivores = table2array(readtable(fullfile(csvFolder, baseTrainingNames{2})));
baseCarnivores = table2array(readtable(fullfile(csvFolder, baseTrainingNames{1})));
    
grouped = getGroupedFileNames(csvFolder);

for k=1:size(grouped, 2)
    herbivores = table2array(readtable(fullfile(csvFolder, grouped{k}{2})));
    carnivores = table2array(readtable(fullfile(csvFolder, grouped{k}{1})));
    
    p1 = plot(baseHerbivores(:, 2), baseHerbivores(:, 3) * timeStep, 'g', baseCarnivores(:, 2), baseCarnivores(:, 3) * timeStep, 'm', ...
        herbivores(:, 2), herbivores(:, 3) * timeStep, 'y', carnivores(:, 2), carnivores(:, 3) * timeStep, 'r');
   
    hold on
    p2 = plot(baseHerbivores(:, 2), smooth(baseHerbivores(:, 3) * timeStep, smoothing), 'g', baseCarnivores(:, 2), smooth(baseCarnivores(:, 3) * timeStep, smoothing), 'm', ...
        herbivores(:, 2), smooth(herbivores(:, 3) * timeStep, smoothing), 'y', carnivores(:, 2), smooth(carnivores(:, 3) * timeStep, smoothing), 'r');
    hold off
    
    for m=1:4
        p1(m).Color(4) = alpha;
        p1(m).Annotation.LegendInformation.IconDisplayStyle = 'off';
        p2(m).LineWidth = lineWidth;
    end
    
    legend('Roślinożercy - model bazowy', 'Mięsożercy - model bazowy', 'Roślinożercy', 'Mięsożercy');
    xlabel('Krok');
    ylabel('Czas życia [s]');
    
    indexes = strfind(grouped{k}{1}, '_');
    fileName = grouped{k}{1}(indexes(1) + 1:(indexes(end-1))-1);
    
    print(fullfile(epsFolder, fileName), '-depsc');
end

p1 = plot(baseHerbivores(:, 2), baseHerbivores(:, 3) * timeStep, 'y', baseCarnivores(:, 2), baseCarnivores(:, 3) * timeStep, 'r');
hold on
p2 = plot(baseHerbivores(:, 2), smooth(baseHerbivores(:, 3) * timeStep, smoothing), 'y', baseCarnivores(:, 2), smooth(baseCarnivores(:, 3) * timeStep, smoothing), 'r');
hold off 

for m=1:2
    p1(m).Color(4) = alpha;
    p1(m).Annotation.LegendInformation.IconDisplayStyle = 'off';
    p2(m).LineWidth = lineWidth;
end

legend('Roślinożercy', 'Mięsożercy');
xlabel('Krok');
ylabel('Czas życia [s]');

print(fullfile(epsFolder, 'Engineering_base_existentialReward'), '-depsc');


function grouped = getGroupedFileNames(csvFolder)
    learningFiles = struct2cell(dir(fullfile(csvFolder, '*.csv')));

    learningFilesNames = learningFiles(1, :);

    tmpGrouping = {};
    for k=1:size(learningFilesNames, 2)
        fileName = learningFilesNames{k};
        indexes = strfind(fileName, '_');
        tmpGrouping{k} = fileName(1:(indexes(end-1)));
    end

    group = findgroups(tmpGrouping);
    grouped = splitapply(@(rows) {rows}, learningFilesNames, group);
end

function smoothed = smooth(input, weight)
    smoothed = zeros(size(input, 1), 1);
    smoothed(1) = input(1);
    for k=2:size(input, 1)
        smoothed(k) = smoothed(k - 1) * weight + (1 - weight) * input(k);
    end
end
    


