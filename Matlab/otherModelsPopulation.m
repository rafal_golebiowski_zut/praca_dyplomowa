csvFolder = 'populationHistories/csv';
epsFolder = 'populationHistories/eps';

sampling = 5;
timeStep = 0.02;
historySize = 30000;

populationHistoriesFileNames = {'populationHistory_foodEaten.csv', 'populationHistory_lowerReproduceReward.csv', 'populationHistory_lowEyeSight.csv', 'populationHistory_higherEyesight.csv'};

for k=1:size(populationHistoriesFileNames, 2)
    subplot(2, 2, k)
    populationHistory = table2array(readtable(fullfile(csvFolder, populationHistoriesFileNames{k})));
    if(size(populationHistory, 1) > historySize)
        populationHistory = populationHistory(1:historySize, :);
    end
    
    x = 0:timeStep:(size(populationHistory, 1) - 1) * timeStep;
%     figure
    plot(x(1:sampling:end), populationHistory(1:sampling:end, 1)', 'g', ...
        x(1:sampling:end), populationHistory(1:sampling:end, 2)', 'y', ...
        x(1:sampling:end), populationHistory(1:sampling:end, 3)', 'r');
%     legend('Rośliny', 'Roślinożercy', 'Mięsożercy');
    xlabel('Czas [s]');
    ylabel('Populacja');
    title(sprintf('Model %d', k));
end

print(fullfile(epsFolder, 'populationHistory_otherModels'), '-depsc');

    


