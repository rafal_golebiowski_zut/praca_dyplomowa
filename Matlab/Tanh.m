x = linspace(-10, 10, 1000);

y = (exp(x) - exp(-x)) ./ (exp(x) + exp(-x));

plot(x, y);
ylim([-1.04, 1.04]);
print -depsc Tanh