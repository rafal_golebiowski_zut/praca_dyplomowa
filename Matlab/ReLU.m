x = linspace(-10, 10, 1000);

y = max(0, x);

plot(x, y);
ylim([-0.2, 10.2]);
print -depsc ReLU