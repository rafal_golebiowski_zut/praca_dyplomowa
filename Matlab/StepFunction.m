x = linspace(-10, 10, 1000);

y = x > 0;

plot(x, y);
ylim([-0.02, 1.02]);
print -depsc StepFunction