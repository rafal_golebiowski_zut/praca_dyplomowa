# https://www.youtube.com/watch?v=i94OvYb6noo&list=PLkt2uSq6rBVctENoVBg1TpCC7OQi31AlC&index=4

import numpy as np

X = np.array([ [0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 1] ])
Y = np.array([[0, 1, 1, 0]]).T

syn0 = 2 * np.random.random((3, 5)) - 1
syn1 = 2 * np.random.random((5, 1)) - 1

for i in range(100):
    l1 = 1 / (1 + np.exp(-(np.dot(X, syn0))))
    l2 = 1 / (1 + np.exp(-(np.dot(l1, syn1))))

    l2Delta = (Y - l2) * (l2 * (1 - l2))
    l1Delta = l2Delta.dot(syn1.T) * (l1 * (1 - l1))

    syn1 += l1.T.dot(l2Delta)
    syn0 += X.T.dot(l1Delta)
