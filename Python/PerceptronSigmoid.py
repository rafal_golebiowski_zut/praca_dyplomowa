import numpy as np

X = np.array([ [1, 2], [0, 1], [2, 0], [0, 2] ])
Y = np.array([[1, 2, 3, 1.5]]).T

weights1 = np.array([ [-0.1, 0.1], [-0.2, 0.2] ]).T
weights2 = np.array([ [0.2, 0.3], [0.1, 0.2], [0.1, 0.2] ]).T
weights3 = np.array([ [0.3, -0.2, 0.1] ]).T

bias1 = np.array([[0.1, 0.2]])
bias2 = np.array([[-0.1, 0.1, -0.2]])
bias3 = np.array([[-0.1]])


f = lambda x : 1 / (1 + np.exp(-x))
fgrad = lambda x : f(x) * (1 - f(x))

eta = 0.1

for i in range(10000):
    z1 = np.dot(X, weights1) + bias1
    out1 = f(z1)
    z2 = np.dot(out1, weights2) + bias2
    out2 = f(z2)
    z3 = np.dot(out2, weights3) + bias3
    out3 = f(z3)

    delta3 = (Y - out3) * fgrad(z3)
    if(i % 100 == 0):
        print(np.average(delta3))

    delta2 = np.dot(delta3, weights3.T) * fgrad(z2)
    delta1 = np.dot(delta2, weights2.T) * fgrad(z1)

    weights3 += np.dot(out2.T, delta3) * eta
    weights2 += np.dot(out1.T, delta2) * eta
    weights1 += np.dot(X.T, delta1) * eta

    bias3 += np.sum(delta3, 0) * eta
    bias2 += np.sum(delta2, 0) * eta
    bias1 += np.sum(delta1, 0) * eta


