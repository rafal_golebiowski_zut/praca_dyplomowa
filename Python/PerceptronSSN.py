import numpy as np

X = np.array([ [1, 1, 2], [1, 0, 1], [1, 2, 0] ])
Y = np.array([[1, 2, 3]]).T

weights1 = np.array([ [0.1, -0.1, 0.1], [0.2, -0.2, 0.2] ]).T
weights2 = np.array([ [-0.1, 0.2, 0.3], [0.1, 0.1, 0.2], [-0.2, 0.1, 0.2] ]).T
weights3 = np.array([ [ -0.1, 0.3, -0.2, 0.1] ]).T

f = lambda x : x
fgrad = lambda x : np.ones_like(x)


for i in range(100):
    out1 = f(np.dot(X, weights1))
    out2 = f(np.dot(np.hstack((np.ones((out1.shape[0], 1)), out1)), weights2))
    out3 = f(np.dot(np.hstack((np.ones((out2.shape[0], 1)), out2)), weights3))

    loss = Y - out3
    loss3 = np.dot(loss, weights3)
    loss2 = np.dot(out2.T, fgrad(loss3))
    loss1 = np.dot(out1.T, fgrad(loss2))

