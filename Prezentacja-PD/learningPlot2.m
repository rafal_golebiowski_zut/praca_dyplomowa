csvFolder = 'learning/csv';
epsFolder = 'learning/eps2';

timeStep = 0.04;

baseTrainingNames = {'run_Engineering_existentialReward_3_0_CarnivoreBrain-tag-Environment_Episode Length.csv', ...
    'run_Engineering_existentialReward_3_0_HerbivoreBrain-tag-Environment_Episode Length.csv'};
baseHerbivores = table2array(readtable(fullfile(csvFolder, baseTrainingNames{2})));
baseCarnivores = table2array(readtable(fullfile(csvFolder, baseTrainingNames{1})));

noNormalizedTrainingNames = {'run_Engineering_existentialReward_no_normalize_1_0_CarnivoreBrain-tag-Environment_Episode Length.csv', ...
    'run_Engineering_existentialReward_no_normalize_1_0_HerbivoreBrain-tag-Environment_Episode Length.csv'};
noNormalizedHerbivores = table2array(readtable(fullfile(csvFolder, noNormalizedTrainingNames{2})));
noNormalizedCarnivores = table2array(readtable(fullfile(csvFolder, noNormalizedTrainingNames{1})));

base2MTrainingNames = {'run_Engineering_existentialReward_2M_1_0_CarnivoreBrain-tag-Environment_Episode Length.csv', ...
    'run_Engineering_existentialReward_2M_1_0_HerbivoreBrain-tag-Environment_Episode Length.csv'};
base2MHerbivores = table2array(readtable(fullfile(csvFolder, base2MTrainingNames{2})));
base2MCarnivores = table2array(readtable(fullfile(csvFolder, base2MTrainingNames{1})));



plot(noNormalizedHerbivores(:, 2), noNormalizedHerbivores(:, 3) * timeStep, 'g', noNormalizedCarnivores(:, 2), noNormalizedCarnivores(:, 3) * timeStep, 'r');
    
legend('Roślinożercy', 'Mięsożercy');
xlabel('Krok');
ylabel('Długość epizodu [s]');

print(fullfile(epsFolder, 'no_normalize'), '-depsc');

plot(baseHerbivores(:, 2), baseHerbivores(:, 3) * timeStep, 'g', baseCarnivores(:, 2), baseCarnivores(:, 3) * timeStep, 'r', ...
        noNormalizedHerbivores(:, 2), noNormalizedHerbivores(:, 3) * timeStep, 'y', noNormalizedCarnivores(:, 2), noNormalizedCarnivores(:, 3) * timeStep, 'b');
    
legend('Roślinożercy - model znormalizowany', 'Mięsożercy - model znormalizowany', 'Roślinożercy', 'Mięsożercy');
xlabel('Krok');
ylabel('Długość epizodu [s]');

print(fullfile(epsFolder, 'no_normalize_vs_normalize'), '-depsc');

plot(base2MHerbivores(:, 2), base2MHerbivores(:, 3) * timeStep, 'g', base2MCarnivores(:, 2), base2MCarnivores(:, 3) * timeStep, 'r');
    
legend('Roślinożercy', 'Mięsożercy');
xlabel('Krok');
ylabel('Długość epizodu [s]');

print(fullfile(epsFolder, 'normalize2M'), '-depsc');
