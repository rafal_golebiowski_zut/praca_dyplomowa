%----------------------------------------------------------------------------------------
%	CHAPTER 2
%----------------------------------------------------------------------------------------

\chapter{Wprowadzenie teoretyczne}
\section{Uczenie maszynowe}
Uczenie maszynowe \cite{sasMachineLearning} jest dziedziną sztucznej inteligencji, zajmującą się tworzeniem algorytmów, które pozwalają maszynom uczyć się samodzielnie. W przeciwieństwie do standardowego podejścia, gdzie najpierw trzeba opracować algorytm, a następnie go przetestować, uczenie maszynowe pozwala na nauczenie się pewnych wzorców, strategii zachowania jedynie na podstawie przekazanych danych. Często można spotkać takie problemy, których rozwiązanie standardowymi metodami byłoby bardzo trudne lub niemożliwe. Przykładem takiego problemu jest wykrywanie twarzy na obrazach. DeepFace \cite{deepFace} — algorytm opracowany przez Facebook'a osiąga w~tym zadaniu ponad 97\% skuteczności.

\noindent Rozwój technologii pozwolił na wykorzystanie metod wymagających dużych mocy obliczeniowych. Stało się możliwe pokonanie człowieka w tradycyjnych grach takich jak szachy i go \cite{alphaZero} oraz w grach komputerowych: Dota 2 \cite{openai2019dota} i StarCraft II \cite{starCraftGrandmaster}.

\subsection{Rodzaje uczenia maszynowego}
Uczenie maszynowe możemy podzielić na 3 główne kategorie:
\begin{itemize}
	\item uczenie nadzorowane — polega na uczeniu za pomocą przykładów. Systemowi zostają przedstawione dane i odpowiadające im etykiety, których musi się nauczyć. Po wytrenowaniu system powinien umieć dobrać prawidłową etykietę do danych przekazanych na wejście algorytmu, nawet jeśli wcześniej nie widział tych danych (generalizacja). Przykładem wykorzystania tego typu uczenia może być klasyfikacja wiadomości e-mail jako ważne lub spam.
	\item uczenie nienadzorowane — algorytm nie otrzymuje informacji o etykietach i~sam musi wyszukać powiązania między danymi. Może być wykorzystane do grupowania klientów o podobnych cechach. 
	\item uczenie ze wzmocnieniem — polega na uczeniu poprzez doświadczenie, metodą prób i błędów. Agent znajduje się w pewnym środowisku, z~którego pobiera dane (obserwacje), na podstawie których podejmuje decyzję, jaką akcję należy wykonać w danym momencie. Następnie otrzymuje informację zwrotną od środowiska w postaci nagrody lub kary. Celem agenta jest maksymalizacja nagród. Ten typ uczenia zostanie zastosowany w tej pracy i zostanie dokładniej omówiony w osobnym rozdziale.
\end{itemize}

\section{Sieci neuronowe}
Jednym z wielu sposobów uczenia maszynowego są sztuczne sieci neuronowe zainspirowane działaniem prawdziwych neuronów \cite{firstNeurons_mcculloch_pitts_1943}. Podstawowym elementem takich sieci są sztuczne neurony (Rys. \ref{fig:neuron}) \cite{nielsen_2015} składające się z~następujących elementów:
\begin{itemize}
	\item wejścia,
	\item wagi,
	\item funkcja aktywacji,
	\item wyjście.
\end{itemize}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth, trim=0 540 0 0, clip]{Neuron}
	\caption{Sztuczny neuron}
	\label{fig:neuron}
\end{figure}
Po podaniu danych na wejścia neuronu obliczana jest wartość $z$ według wzoru:
\begin{equation}
	z = \sum_{i=1}^{n}w_i \cdot x_i + b = w \cdot x + b\,,
\end{equation}
gdzie $x$ to dane wejściowe, $w$ to wagi, a $b$ to specjalna waga zawsze mnożona przez 1~niezależnie od danych wejściowych. \smallskip \\ 
Następnie ta wartość przekazywana jest do funkcji aktywacji, której zadaniem jest powiedzenie, jak bardzo neuron został pobudzony. Perceptron Rossenblatta \cite{rosenblatt_1958}, jeden z pierwszych modeli sztucznego neuronu wykorzystywał funkcję skokową, w której bias był wartością progową:
\begin{equation}
	f(z) = 
	\begin{cases}
		{0,} & {\text { gdy } z \leq 0} \\
		{1,} & {\text { gdy } z > 0}
	\end{cases}\,.
\end{equation}
Dobór funkcji aktywacji zależy od rozwiązywanego problemu. Kilka najpopularniejszych pokazano na rysunku \ref{fig:ActivationFunctions}, a ich wzory podano poniżej:
\begin{itemize}
	\item funkcja sigmoidalna
	\begin{equation}
		f(z) = \frac{1}{1 + e^{-\beta z}}\,,
	\end{equation}
	\item tangens hiperboliczny
	\begin{equation}
		f(z) = \frac{e^{\beta z} - e^{-\beta z}}{e^{\beta z} + e^{-\beta z}}\,,
	\end{equation}
	\item ReLU (rectified linear unit)
	\begin{equation}
		f(z) = max(0, z)\,.
	\end{equation}
\end{itemize}
\begin{figure}[h!]
	\centering
	\begin{subfigure}[h]{0.45\linewidth}
		\centering
		\includegraphics[width=1\linewidth]{StepFunction}
		\caption{Funkcja skokowa}
		\label{fig:StepFunction}
	\end{subfigure}
	\begin{subfigure}[h]{0.45\linewidth}
		\centering
		\includegraphics[width=1\linewidth]{Sigmoid}
		\caption{Funkcja sigmoidalna}
		\label{fig:Sigmoid}
	\end{subfigure}
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=1\linewidth]{Tanh}
		\caption{Tangens hiperboliczny}
		\label{fig:Tanh}
	\end{subfigure}
	\begin{subfigure}[h]{0.45\textwidth}
		\includegraphics[width=1\linewidth]{ReLU}
		\caption{ReLU}
		\label{fig:ReLU}
	\end{subfigure}

	\caption{Przykładowe funkcje aktywacji}
	\label{fig:ActivationFunctions}
\end{figure}
\noindent Pojedynczy neuron potrafi rozwiązać tylko bardzo proste problemy \cite{minsky_1969}. Do trudniejszych zadań wykorzystuje się wielowarstwowe sieci neuronowe.

\subsection{Wielowarstwowe sieci neuronowe}
Są to sieci zbudowane z kilku warstw (Rys. \ref{fig:MLP}). Na każdej warstwie znajduje się wiele neuronów posiadających tę samą funkcję aktywacji.
\begin{figure}[hb!]
	\centering
	\includegraphics[width=0.9\linewidth]{MLP}
	\caption{Wielowarstwowa sieć neuronowa}
	\label{fig:MLP}
\end{figure}
Aby poznać odpowiedź sieci, należy obliczyć odpowiedzi neuronów na wszystkich poprzednich warstwach \cite{nielsen_2015, kang_2019}. Aby zrobić to efektywnie, stosuje się wersję macierzową i~oblicza wyjścia dla całej warstwy jednocześnie: 

\begin{align}
	&z^{(l)} = 
	\begin{bmatrix}
		w_{0, 0}^{(l)} & w_{0, 1}^{(l)} & ... & w_{0, n}^{(l)}\\
		w_{1, 0}^{(l)} & w_{1, 1}^{(l)} & ... & w_{1, n}^{(l)}\\
		... & ... & ... & ...\\
		w_{k, 0}^{(l)} & w_{k, 1}^{(l)} & ... & w_{k, n}^{(l)}\\
	\end{bmatrix} 
	\begin{bmatrix}
		a_0^{(l-1)}\\
		a_1^{(l-1)}\\
		...\\
		a_n^{(l-1)}\\
	\end{bmatrix}
	+
	\begin{bmatrix}
		b_0^{(l)}\\
		b_1^{(l)}\\
		...\\
		b_n^{(l)}\\
	\end{bmatrix}\,,\\	
	&z^{(l)} = w^{(l)} a^{(l-1)} + b^{(l)}\,,
	\label{eq:layerSum}	\\
	&a^{(l)} = f(z^{(l)})\,,
	\label{eq:layerResponse}
\end{align}
gdzie $w^{(l)}$ to wagi między warstwą $l$ i $l-1$, $a^{(l)}$ to odpowiedzi neuronów z warstwy $l$, $b^{(l)}$ to bias neuronów z warstwy $l$, $k$ to liczba neuronów w warstwie $l$, $n$ to liczba neuronów w warstwie $l-1$.

\subsection{Uczenie sieci neuronowych}
Do prawidłowego działania sieci neuronowych wymagane jest dobranie odpowiednich wag, których sieć musi się nauczyć w trakcie treningu. Jedną z najczęściej stosowanych metod jest algorytm wstecznej propagacji błędu \cite{nielsen_2015, karpathyBackpropagation, missinglinkBackpropagation, kostadinov_2019}.
Uczenie nadzorowane sieci tą metodą przedstawiono poniżej:
\begin{lstlisting}[mathescape, caption=Uczenie nadzorowane metodą wstecznej propagacji błędu, label=listing:backPropagation] 
Zainicjalizuj wagi losowymi wartościami.
Dla każdej epoki:
	Wymieszaj próbki.
	Dla każdej próbki $(x, y)$:
		Oblicz wyjście z sieci $a^{(L)}$.
		Oblicz błąd sieci $C(y, a^{(L)})$.
		Oblicz błąd uogólniony dla każdej warstwy $\delta^{(l)}$.
		Oblicz wartość poprawki wag dla każdej warstwy $\Delta W^{(l)}$.
		Skoryguj wagi i bias.
\end{lstlisting}
Wyjście z sieci można obliczyć, korzystając ze wzorów \eqref{eq:layerSum} i \eqref{eq:layerResponse}. Błąd sieci jest obliczany za pomocą funkcji kosztu~$C$. Przykładem takiej funkcji jest błąd kwadratowy:
\begin{equation}
	C(y, a^{(L)}) = \frac{1}{2}\sum_{i=0}{(y_i - a^{(L)}_i})^2\,,
	\label{eq:MSE}
\end{equation}
gdzie $y$ to oczekiwana odpowiedź, $a^{(L)}$ to odpowiedź sieci neuronowej, $i$ to numer neuronu na ostatniej warstwie. Ta funkcja posiada bardzo prosty do obliczenia gradient, czyli wektor pochodnych cząstkowych $\frac{\partial C}{\partial a^{(L)}_i}$, potrzebny do obliczenia $\delta^{(L)}$:
\begin{equation}
	\nabla_{a^{(L)}}C = a^{(L)} - y\,.
\end{equation}

\newpage
\noindent Dla ostatniej warstwy wartość $\delta^{(L)}$ to:
\begin{equation}
	\delta^{(L)} = \nabla_{a^{(L)}}C \odot f'(z^{(L)})\,,
\end{equation}
gdzie $f'(z^{(L)})$ to pochodna funkcji aktywacji. \smallskip \\
Dla każdej kolejnej warstwy:
\begin{equation}
	\delta^{(l)} = ((W^{(l+1)})^T 	\delta^{(l + 1)} \odot f'(z^{(l)})\,.
\end{equation}
Następnie oblicza się wartość poprawki:
\begin{equation}
	\Delta W^{(l)} = (a^{(l - 1)})^T \delta^{(l)}\,.
\end{equation}
Ostatnim krokiem jest korekcja wag i bias'u:
\begin{align}
	&W^{(l)} = W^{(l)} + \eta \Delta W^{(l)}\,,\\
	&b^{(l)} = b^{(l)} + \eta \delta^{(l)}\,,
\end{align}
gdzie $\eta$ to współczynnik uczenia, zazwyczaj bardzo mała liczba np. 0.01.

\section{Uczenie ze wzmocnieniem}
Uczenie ze wzmocnieniem \cite{sasMachineLearning, spinningupOpenAi, deepRL_MIT} polega na nauce metodą prób i błędów. Agent znajduje się w pewnym środowisku i zbiera dane o aktualnym stanie $s$. Na podstawie tych obserwacji wykonuje akcję $a$ zgodnie ze swoją strategią $\pi$, za co dostaje nagrodę lub karę $r$. Schemat działania tego rodzaju uczenia pokazano na rysunku \ref{fig:reinforcementLearning}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{ReinforcementLearning}
	\caption{Schemat uczenia ze wzmocnieniem}
	\label{fig:reinforcementLearning}
\end{figure}
Celem algorytmu uczenia ze wzmocnieniem jest dobranie takiej strategii, która zapewni maksymalizację nagród agenta. Strategia jest funkcją, która zwraca prawdopodobieństwo dla każdej pary akcja-stan \eqref{eq:policyAS} lub akcję dla danego stanu \eqref{eq:policyS}, w zależności jak zdefiniujemy problem.
\begin{align}
	&\pi(a | s) \rightarrow [0, 1]
	\label{eq:policyAS}\\
	&a = \pi(s)
	\label{eq:policyS}
\end{align}

\subsection{Policy gradient}
Policy gradient \cite{weng_2018, schulmanDeepRL, spinningupOpenAi, deepRL_MIT} jest podstawowym algorytmem optymalizującym strategię $\pi$. Najczęściej jest ona realizowana przez sieć neuronową, której wagi to $\theta$,~a~funkcja kosztu to:
\begin{equation}
	L_{PG}(\theta) = \hat{E}_t\left[log\pi_\theta\left(a_t|s_t\right)\hat{\Psi}_t\right]\,,
\end{equation}
gdzie $\hat{\Psi}_t$ jest funkcją zwracającą wartość wzmocnienia.
Najczęściej $\hat{\Psi}$ przyjmuje jedną z następujących form \cite{schulman2015highdimensional}:
\begin{itemize}
	\item zdyskontowana suma przyszłych nagród:
	\begin{equation}
		R_t = \sum_{k=0}^{\infty}\gamma^kr_{t+k}\,,
	\end{equation}
	gdzie $\gamma$ to współczynnik dyskontowania.
	\item różnica między zaobserwowaną a spodziewaną nagrodą:
	\begin{equation}
		\hat{A}_t = R_t - V(s_t)\,,
		\label{eq:advantageFunction}
	\end{equation}
	gdzie $V(s_t)$ to wartość spodziewana dla danego stanu.
	\item Generalized Advantage Estimation (GAE):
	\begin{align}
		&\delta_t = r_t + \gamma V(s_{t+1}) - V(s_t)\,,\\
		&\hat{A}_t^{GAE} = \sum_{k=0}^{\infty}(\gamma\lambda)^k \delta_{t+k}\,,
		\label{eq:GAE}
	\end{align}
	gdzie $\gamma$ to współczynnik dyskontowania, $\lambda$ to współczynnik odpowiedzialny za redukowanie wariancji.
\end{itemize}
Pojedynczą iterację tego algorytmu w środowisku uczącym można opisać w następujący sposób:
\begin{lstlisting}[caption=Policy gradient, label=listing:policyGradient] 
Przez T kroków zbieraj trajektorie ($a_t, s_t, r_t$) używając strategii $\pi_{\theta_{old}}$.
Oblicz wartość wzmocnienia $\hat{\Psi}_{1}$, ..., $\hat{\Psi}_{T}$.
Optymalizuj funkcję kosztu L w odniesieniu do $\theta$ przez K epok.
\end{lstlisting}

\subsection{Proximal Policy Optimization}
Proximal Policy Optimization (PPO) \cite{schulman2017proximal, schulmanPPO, spinningupOpenAi, weng_2018} to wydajna i łatwa w implementacji metoda zaprezentowana w 2017 roku. Bazuje na Policy Gradient, ale zamiast $log\pi_\theta(a_t|s_t)$ wykorzystywany jest stosunek aktualnie optymalizowanej strategii i~strategii, przy pomocy której zebrano dane:
\begin{equation}
	r_t(\theta) = \frac{\pi_\theta(a_t | s_t)}{\pi_{\theta_{old}}(a_t | s_t)}\,.
\end{equation}
Funkcja kosztu wygląda następująco:
\begin{equation}
	L^{CLIP}(\theta) = \hat{E}_t\left[min(r_t(\theta)\hat{\Psi}_t,\, clip(r_t(\theta),\, 1 - \epsilon,\, 1 + \epsilon)\hat{\Psi}_t\right]\,,
\end{equation}
gdzie $\epsilon$ jest parametrem ograniczającym zmiany do niewielkiej przestrzeni wokół $\pi_{\theta_{old}}$. $\hat{\Psi}$ to najczęściej funkcja przewagi \eqref{eq:advantageFunction} lub GAE \eqref{eq:GAE}.\\
Gdy $\hat{\Psi} > 0$ zmiany są ograniczone do 1 + $\epsilon$.\\
Gdy $\hat{\Psi} < 0$ zmiany są ograniczone do 1 - $\epsilon$. 

%\begin{figure}[h]
%	\centering
%	\includegraphics[width=1\linewidth]{PPO_Clip}
%	\caption{Ilustracja działania funkcji $L^{CLIP}$}
%\end{figure}

